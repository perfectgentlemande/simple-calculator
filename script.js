//Привязка обработчиков
function initializeCalculator()
{
    buttonC.addEventListener("click", buttonC_click);
    buttonOne.addEventListener("click", buttonOne_click);
    buttonTwo.addEventListener("click", buttonTwo_click);
    buttonThree.addEventListener("click", buttonThree_click);
    buttonFour.addEventListener("click", buttonFour_click);
    buttonFive.addEventListener("click", buttonFive_click);
    buttonSix.addEventListener("click", buttonSix_click);
    buttonSeven.addEventListener("click", buttonSeven_click);
    buttonEight.addEventListener("click", buttonEight_click);
    buttonNine.addEventListener("click", buttonNine_click);
    buttonZero.addEventListener("click", buttonZero_click);
    buttonInverse.addEventListener("click", buttonInverse_click);
    buttonToDouble.addEventListener("click", buttonToDouble_click);
    enableOperators();
}

//Очистка всего
function clear() {
    operandAIsComplete = false;
    operandBIsComplete = false;
    operandA="0";
    operandB="0";
    switchedToAdd=false;
    switchedToSubstract=false;
    switchedToMultiply=false;
    switchedToDivide=false;
    enableOperators();
}

//Отключение операторов
function enableOperators() {
    buttonAdd.addEventListener("click", buttonAdd_click);
    buttonSubstract.addEventListener("click", buttonSubstract_click);
    buttonMultiply.addEventListener("click", buttonMultiply_click);
    buttonDivide.addEventListener("click", buttonDivide_click);
    buttonCount.removeEventListener("click", buttonCount_click); 
}

//Включение операторов
function disableOperators() {
    buttonAdd.removeEventListener("click", buttonAdd_click);
    buttonSubstract.removeEventListener("click", buttonSubstract_click);
    buttonMultiply.removeEventListener("click", buttonMultiply_click);
    buttonDivide.removeEventListener("click", buttonDivide_click);
    buttonCount.addEventListener("click", buttonCount_click);
}

//Вывод чисел
function showNumber(number) {
    var textBlockOutput=document.getElementById("textBlockOutput");
    while (textBlockOutput.firstChild) {
        textBlockOutput.removeChild(textBlockOutput.firstChild);
    }
    textBlockOutput.appendChild(document.createTextNode(number));
}

//Обработка нажатий кнопок
function buttonC_click(e) {
    operandA="0";
    operandB="0";
    operandAIsComplete=false;
    operandBIsComplete=false;
    showNumber(operandA);
}
function buttonOne_click(e) { 
    if (!operandAIsComplete)
    {
        if (operandA==="0") {
            operandA="1";
        }
        else
        {
            operandA+="1"
        }
        showNumber(operandA);
    }
    else if (!operandBIsComplete)
    {
        if (operandB==="0") {
            operandB="1";
        }
        else
        {
            operandB+="1"
        }
        showNumber(operandB);
    }
}
function buttonTwo_click(e) { 
    if (!operandAIsComplete)
    {
        if (operandA==="0") {
            operandA="2";
        }
        else
        {
            operandA+="2"
        }
        showNumber(operandA);
    }
    else if (!operandBIsComplete)
    {
        if (operandB==="0") {
            operandB="2";
        }
        else
        {
            operandB+="2"
        }
        showNumber(operandB);
    }
}
function buttonThree_click(e) { 
    if (!operandAIsComplete)
    {
        if (operandA==="0") {
            operandA="3";
        }
        else
        {
            operandA+="3"
        }
        showNumber(operandA);
    } 
    else if (!operandBIsComplete)
    {
        if (operandB==="0") {
            operandB="3";
        }
        else
        {
            operandB+="3"
        }
        showNumber(operandB);
    }
}
function buttonFour_click(e) { 
    if (!operandAIsComplete)
    {
        if (operandA==="0") {
            operandA="4";
        }
        else
        {
            operandA+="4"
        }
        showNumber(operandA);
    }
    else if (!operandBIsComplete)
    {
        if (operandB==="0") {
            operandB="4";
        }
        else
        {
            operandB+="4"
        }
        showNumber(operandB);
    }
}
function buttonFive_click(e) { 
    if (!operandAIsComplete)
    {
        if (operandA==="0") {
            operandA="5";
        }
        else
        {
            operandA+="5"
        }
        showNumber(operandA);
    }
    else if (!operandBIsComplete)
    {
        if (operandB==="0") {
            operandB="5";
        }
        else
        {
            operandB+="5"
        }
        showNumber(operandB);
    }
}
function buttonSix_click(e) { 
    if (!operandAIsComplete)
    {
        if (operandA==="0") {
            operandA="6";
        }
        else
        {
            operandA+="6"
        }
        showNumber(operandA);
    }
    else if (!operandBIsComplete)
    {
        if (operandB==="0") {
            operandB="6";
        }
        else
        {
            operandB+="6"
        }
        showNumber(operandB);
    }
}
function buttonSeven_click(e) { 
    if (!operandAIsComplete)
    {
        if (operandA==="0") {
            operandA="7";
        }
        else
        {
            operandA+="7"
        }
        showNumber(operandA);
    }
    else if (!operandBIsComplete)
    {
        if (operandB==="0") {
            operandB="7";
        }
        else
        {
            operandB+="7"
        }
        showNumber(operandB);
    }
}
function buttonEight_click(e) { 
    if (!operandAIsComplete)
    {
        if (operandA==="0") {
            operandA="8";
        }
        else
        {
            operandA+="8"
        }
        showNumber(operandA);
    }
    else if (!operandBIsComplete)
    {
        if (operandB==="0") {
            operandB="8";
        }
        else
        {
            operandB+="8"
        }
        showNumber(operandB);
    }
}
function buttonNine_click(e) { 
    if (!operandAIsComplete)
    {
        if (operandA==="0") {
            operandA="9";
        }
        else
        {
            operandA+="9"
        }
        showNumber(operandA);
    }
    else if (!operandBIsComplete)
    {
        if (operandB==="0") {
            operandB="9";
        }
        else
        {
            operandB+="9"
        }
        showNumber(operandB);
    }
}
function buttonZero_click(e) { 
    if (!operandAIsComplete)
    {
        if (operandA!=="0") {
            operandA+="0";
        }
        showNumber(operandA);
    } 
    else if (!operandBIsComplete)
    {
        if (operandB!=="0") {
            operandB+="0";
        }
        showNumber(operandB);
    } 
}
function buttonInverse_click(e) { 
    if (!operandAIsComplete && operandA[0]!=="-")
    {
        operandA="-"+operandA;
        showNumber(operandA);
    }
    else if (!operandAIsComplete && operandA[0]==="-")
    {
        operandA=operandA.replace(/-/,"");
        showNumber(operandA);
    }
    else if (!operandBIsComplete && operandB[0]!=="-")
    {
        operandB="-"+operandB;
        showNumber(operandB);
    }
    else if (!operandBIsComplete && operandB[0]==="-")
    {
        operandB=operandB.replace(/-/,"");
        showNumber(operandB);
    }
}
function buttonAdd_click(e) { 
    operandAIsComplete=true;
    showNumber(operandB);
    if (!operandBIsComplete)
    {
        switchedToAdd=true;
        disableOperators();
    }
}
function buttonSubstract_click(e) { 
    operandAIsComplete=true;
    showNumber(operandB);
    if (!operandBIsComplete)
    {
        switchedToSubstract=true;
        disableOperators();
    }
}
function buttonMultiply_click(e) { 
    operandAIsComplete=true;
    showNumber(operandB);
    if (!operandBIsComplete)
    {
        switchedToMultiply=true;
        disableOperators();
    }
}
function buttonDivide_click(e) { 
    operandAIsComplete=true;
    showNumber(operandB);
    if (!operandBIsComplete)
    {
        switchedToDivide=true;
        disableOperators();
    }
}
function buttonToDouble_click(e) { 
    if (!operandAIsComplete && (operandA.indexOf(".")===-1))
    {
        operandA+=".";
        showNumber(operandA);
    } 
    else if (!operandBIsComplete && (operandB.indexOf(".")===-1))
    {
        operandB+=".";
        showNumber(operandB);
    } 
}
function buttonCount_click(e) { 
    var result;
    if (switchedToAdd)
    {
        result=parseFloat(operandA)+parseFloat(operandB);
        showNumber(result);
    }
    if (switchedToSubstract)
    {
        result=parseFloat(operandA)-parseFloat(operandB);
        showNumber(result);
    }
    if (switchedToMultiply)
    {
        result=parseFloat(operandA)*parseFloat(operandB);
        showNumber(result);
    }
    if (switchedToDivide)
    {
        result=parseFloat(operandA)/parseFloat(operandB);
        showNumber(result);
    }
    clear();
    operandA=""+result;
    showNumber(operandA);
    enableOperators();
}

//Начальные состояния операндов
var operandAIsComplete = false;
var operandBIsComplete = false;
var operandA="0";
var operandB="0";
var switchedToAdd=false;
var switchedToSubstract=false;
var switchedToMultiply=false;
var switchedToDivide=false;

//Получение элементов с помощью DOM
var buttonC = document.getElementById("buttonC");
var buttonDivide = document.getElementById("buttonDivide");
var buttonSeven = document.getElementById("buttonSeven");
var buttonEight = document.getElementById("buttonEight");
var buttonNine = document.getElementById("buttonNine");
var buttonMultiply = document.getElementById("buttonMultiply");
var buttonFour = document.getElementById("buttonFour");
var buttonFive = document.getElementById("buttonFive");
var buttonSix = document.getElementById("buttonSix");
var buttonSubstract = document.getElementById("buttonSubstract");
var buttonOne = document.getElementById("buttonOne");
var buttonTwo = document.getElementById("buttonTwo");
var buttonThree = document.getElementById("buttonThree");
var buttonAdd = document.getElementById("buttonAdd");
var buttonInverse = document.getElementById("buttonInverse");
var buttonZero = document.getElementById("buttonZero");
var buttonToDouble = document.getElementById("buttonToDouble");
var buttonCount = document.getElementById("buttonCount");

//Начало
initializeCalculator();
showNumber(operandA);
